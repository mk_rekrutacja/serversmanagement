import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { ServersListComponent } from './components/servers-list/servers-list.component';

  const routes: Routes = [
      {
          path: '',
          component: AppComponent,
      }
    ];

  @NgModule({
      imports: [
          RouterModule.forRoot(routes),
      ],
      exports: [
          RouterModule
      ],
      declarations: []
    })
  export class AppRoutingModule { }