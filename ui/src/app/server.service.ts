import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IServer } from './models/server';
import {​​​​​ environment }​​​​​ from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  private apiUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  public getAllServers(): Observable<IServer[]>{
    return this.http.get<IServer[]>(this.apiUrl + 'servers');
  }

  public getServer(id: number){
    return this.http.get<IServer>(this.apiUrl + 'servers/' + id);
  }

  public turnON (id: number): Observable<IServer>{
    return this.http.put<IServer>(this.apiUrl + 'servers/' + id + '/on', id);
  }

  public turnOFF (id: number): Observable<IServer>{
    return this.http.put<IServer>(this.apiUrl + 'servers/' + id + '/off', id);
  }

  public reboot (id: number): Observable<IServer>{
    return this.http.put<IServer>(this.apiUrl + 'servers/' + id + '/reboot', id);
  }
}
