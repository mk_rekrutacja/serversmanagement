import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ServerService } from 'src/app/server.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  @Input() numberOfServers;
  @Output() searchText = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }
  
  sendText(value: string) {
    this.searchText.emit(value.toString());
    console.log(value);
  }
}
