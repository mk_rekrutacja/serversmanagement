import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ServerService } from 'src/app/server.service';
import { ServerComponent } from './server.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { HttpClientModule } from '@angular/common/http';

describe('ServerComponent', () => {
  let component: ServerComponent;
  let fixture: ComponentFixture<ServerComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServerComponent ],
      imports: [
        MatMenuModule,
        MatButtonModule,
        MatIconModule,
        HttpClientModule
    ],
    providers: [ServerService],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should return rebooting server status', (done: DoneFn) => {
    let component = TestBed.get(ServerService);
    let mockServer = {
      id: 1,
      name: "test name",
      status: "test status"
    };
    component.reboot(mockServer.id).subscribe(value => {
    expect(value.status).toBe("REBOOTING");
    done();
    });
  });

  it('should reboot server', () => {
    let component = TestBed.get(ServerService);
    let mockServer = {id: 1, name: "test name", status: "test status"};
      component.getServer(mockServer.id).subscribe(data => {
        expect(data.status).toBe("ONLINE"); 
      });
    });
});

