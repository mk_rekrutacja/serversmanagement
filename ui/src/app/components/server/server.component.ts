import { Component, Input, OnInit } from '@angular/core';
import { ServerService } from 'src/app/server.service';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss']
})
export class ServerComponent implements OnInit {

  @Input() serverID: number;
  @Input() serverName: any;
  @Input() serverStatus: any;

  constructor(private serverService: ServerService) { }

  ngOnInit() {
  }

  turnON() {
    this.serverService.turnON(this.serverID).subscribe(data => {
      this.serverStatus = data.status;
    });
  }

  turnOFF() {
    this.serverService.turnOFF(this.serverID).subscribe(data => {
      this.serverStatus = data.status;
    });
  }

  reboot() {
    this.serverService.reboot(this.serverID).subscribe (x => {
      this.serverStatus = x.status + "...";
    });
    setInterval(() => {
        this.serverService.getServer(this.serverID).subscribe(data => {
          if (data.status === "ONLINE") {
            this.serverStatus = data.status;
          }
        })
        }, 1000);
  }
}
