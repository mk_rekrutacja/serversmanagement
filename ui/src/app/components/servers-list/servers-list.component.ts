import { HttpClient } from '@angular/common/http';
import { ReturnStatement } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { interval } from 'rxjs/internal/observable/interval';
import { IServer } from 'src/app/models/server';
import { ServerService} from '../../server.service';

@Component({
  selector: 'app-servers-list',
  templateUrl: './servers-list.component.html',
  styleUrls: ['./servers-list.component.scss']
})
export class ServersListComponent implements OnInit {

  servers$: Observable<IServer[]>;
  countServers: number;
  filterObjects: IServer[] = [];
  text: string;

  constructor(private serverService: ServerService) { }

  ngOnInit() {
    this.getAllServers();
  }

  getAllServers() {
    this.servers$ = this.serverService.getAllServers();
    this.servers$.subscribe(servers => {
      this.countServers = servers.length;
    })
  }

  filterList(searchText: string) {
     this.text = searchText;
  }
}
